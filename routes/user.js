const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth")

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send (resultFromController))
})

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController))
})

// Session 33 Mini-Activity

/*router.post("/details", (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send (resultFromController))
})*/


// Session 34
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile(req.body).then(resultFromController => res.send (resultFromController))
})

// Session 36
// For enrolling a user
router.post("/enroll", auth.verify, (req, res) => {
	let loggedUser = auth.decode(req.headers.authorization)

	let data = {
		userId: loggedUser.id,
		courseId: req.body.courseId
	}

	if (loggedUser.isAdmin){
		res.send(`User is admin, prohibited in enrolling in any course.`)
	} else {
	userController.enroll(data).then(resultFromController => res.send(`${resultFromController}. Successfully enrolled!`))
	}
})

module.exports = router
